# Instructions to run

### create database

- create mongo db `docker run --name mongo-local -d -p 27017:27017 -v </your/local/directory>:/data/db mongo`

- connect to mongo database `docker exec -it mongo-local bash -l`

- login to mongo `mongo`

- create mongo database `use testDB` check database connection `db.getName()`

- add user to mongo database `db.createUser({user: "admin", pwd: "password", roles: [ "readWrite", "dbAdmin" ]})`

### run application

- copy the `{root}/node-app/src/config/database-config.example.json` and create `database-config.json`

- fill up the database information

- copy the `{root}/backend/node-app/src/app.env.example` and create `app.env`

- fill up the env you want the application to run

- go to `{root}`

- run `docker-compose build`

- run `docker-compose up`

- open the application on `localhost:8383`
